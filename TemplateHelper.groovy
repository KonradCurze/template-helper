/*
* Finds all placeholders in one or two XML-templates.
* Shows placeholders in common and for each template separately.
* */

/*
* Main block.
* */
if (args) {
    switch (args.size()) {
        case 1:
            def template = new File(args.first())
            printPlaceholders(template)
            break
        case 2:
            def first = new File(args.first())
            def second = new File(args.last())
            printPlaceholders(first, second)
            break
        default:
            showUsageAndQuit()
    }
} else {
    showUsageAndQuit()
}

/*
* Functions.
* */
private void showUsageAndQuit() {
    println("USAGE:\ngroovy ${this.class.getSimpleName()}.groovy template_1 [template_2]")
    System.exit(0)
}

private void printPlaceholders(File template) {
    def fileContent = template.getText('utf-8')

    def placeholders = [] as TreeSet
    fileContent.findAll(~/\$\{.*?\}/, {
        placeholders << it
    })

    placeholders.each {
        println it
    }
}

private void printPlaceholders(File first, File second) {
    // Read template #1
    def firstContent = first.getText('utf-8')
    def firstPlaceholders = firstContent.findAll(~/\$\{.*?\}/) as TreeSet
    // Read template #2
    def secondContent = second.getText('utf-8')
    def secondPlaceholders = secondContent.findAll(~/\$\{.*?\}/) as TreeSet
    // Find distinct placeholders
    def distinctPlaceholders = findDistinctPlaceholders(firstPlaceholders, secondPlaceholders)
    // Find joint placeholders
    def jointPlaceholders = findJointPlaceholders(firstPlaceholders, secondPlaceholders, distinctPlaceholders)
    // Remove joint placeholders
    firstPlaceholders.removeAll(jointPlaceholders)
    secondPlaceholders.removeAll(jointPlaceholders)

    // Output
    println '\nJoint:'
    jointPlaceholders.each {
        println it
    }
    println '\nTemplate #1:'
    firstPlaceholders.each {
        println it
    }
    println '\nTemplate #2:'
    secondPlaceholders.each {
        println it
    }
}

private TreeSet findJointPlaceholders(TreeSet<String> firstPlaceholders, TreeSet<String> secondPlaceholders, HashSet<String> distinctPlaceholders) {
    def jointPlaceholders = [] as TreeSet
    jointPlaceholders.addAll(firstPlaceholders)
    jointPlaceholders.addAll(secondPlaceholders)
    jointPlaceholders.removeAll(distinctPlaceholders)
    jointPlaceholders
}

private Set findDistinctPlaceholders(Set a, Set b) {
    def aMinusB = new HashSet(a)
    aMinusB.removeAll(b)

    def bMinusA = new HashSet(b)
    bMinusA.removeAll(a)

    def result = [] as HashSet<String>

    result.addAll(aMinusB)
    result.addAll(bMinusA)

    return result
}