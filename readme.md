#Template helper
This groovy script detects and prints placeholders in the template files.

For instance, you have your 'file1.xml' which contains:
```
<car>
  <engine>
    <code>${engine_code}</code>
    <producer>${engine_producer}</producer>
    <layout>${engine_layout}</layout>
  </engine>
  <transmission>
    <code>${transmission code}</code>
    <producer>${transmission_producer}</producer>
    <type>${transmission_type}</type>
  </transmission>
</car>
```

if you call ```groovy TemplateHelper.groovy file.xml```, you will receive an output like this:
```
${engine_code}
${engine_layout}
${engine_producer}
${transmission code}
${transmission_producer}
${transmission_type}
```

It is also possible to pass up to two files:
'```groovy TemplateHelper.groovy template1.xml template2.xml```',
in this case, the script prints placeholders which these templates have in common and separately.